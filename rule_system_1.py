# python code for initial chtbot rules
from random import randint
import json

# patterns
p0_list = [  "hi" , "hello" , "greetings" ]
p1_list = [ "bye" , "goodbye" , "speak later" , "take care"]
p2_list = [ "haha" , "lol" , "funny"]
p3_list = ["name" ] # or (item from name lib)
p4_list = [ "wrong" , "incorrect" ]
p5_list = [ "" ] # empty
p6_list = [ "yes" , "correct", "yeah" ]
p7_list = [ "no" , "incorrect", "wrong" ]

#names
with open('random-names/first-names.json') as f:
    names = json.load(f)
print(names[:10])



timeout = 10  # seconds --> not used 
cond = True
while(cond):
    x = input("user: ")
    x = x.lower()
    if x == "exit":
        cond = False # break
        break 
    elif any(str_in_list in x for str_in_list in p0_list):
        reply = [ "hi", "hello", "welcome", "good-day"]
        rand = randint(0, len(reply)-1)
        print("bot: ", reply[rand])
    elif any(str_in_list in x for str_in_list in p1_list):
        reply = [ "goodbye", "see you again"]
        rand = randint(0, len(reply)-1)
        print("bot: ", reply[rand])
    elif any(str_in_list in x for str_in_list in p2_list):
        reply =  [ "did you find that funny", "what is so funny", "what are you luaghing at"]
        rand = randint(0, len(reply)-1)
        print("bot: ", reply[rand])
    elif any(str_in_list in x for str_in_list in p3_list) or any(str_in_list in x for str_in_list in names) :
        reply =[ "is that your name", "that is a nice name", "who is _x_"]
        rand = randint(0, len(reply)-1)
        print("bot: ", reply[rand])
    elif any(str_in_list in x for str_in_list in p4_list):
        reply = [ "what should I have said?", "where did I go wrong?"]
        rand = randint(0, len(reply)-1)
        print("bot: ", reply[rand])
    elif x == "": # p5_list but faster to not use the list unless another item is added to it
        reply = ["Have you got nothing more to say?", "sorry I didn't quite get that"]
        rand = randint(0, len(reply)-1)
        print("bot: ", reply[rand])
    elif any(str_in_list in x for str_in_list in p6_list):
        reply = [ "great", "correcto potronum" ]
        rand = randint(0, len(reply)-1)
        print("bot: ", reply[rand])
    elif any(str_in_list in x for str_in_list in p7_list):
        reply = [ "What did I get wrong?" , "What was wrong about that?" ]
        rand = randint(0, len(reply)-1)
        print("bot: ", reply[rand])
    
    
        
