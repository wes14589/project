import socket

# create our udp socket
try:
    socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
except socket.error:
    print("Oops, something went wrong connecting the socket")
    exit()

while 1:
    message = input("User: ")

    # encode the message
    message = message.encode()

    try:
        # send the message
        #socket.sendto(message, ("212.219.220.152", 1000)) # this should be the ip for my VM but isn't working
        #socket.sendto(message, ("10.0.2.2", 10000)) # try this address next!!
        socket.sendto(message, ("192.168.56.1", 10000)) # send the encoded string "meesage" to the IP and Port stated

        # output the response (if any)
        data, ip = socket.recvfrom(1024) # save the respoens from the server

        print("Bot: {}".format(data.decode()))

    except socket.error:
        print("Error! {}".format(socket.error))
        exit()



#code_origin = "https://medium.com/@makerhacks/python-client-and-server-internet-communication-using-udp-c4f5fc608945"