# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:58:15 2019

@author: tomwe
"""

import re
import random
import json


user_msg = "User: "
bot_msg = "Bot: {0}"

message = "do you want to go and play" # example message
reply = "yes let's go and do that" # example reply
default = "Sorry I did not understant that (not a rule, response or question)"
with open('responses.json') as f:
    responses = json.load(f)
with open('rules.json') as f:
    rules = json.load(f)


def match_given_rule(rules, message):
    response , phrase = "default", None # set default results incase nothing is found
    for key, value in rules.items():
        ans = re.search(key, message)
        if ans is not None:
            #print("Match found, responses in dict: ", value)
            response = random.choice(value) # random value in list
            phrase = ans.group(1)
            if '{0}' in response:
                response = response.format(phrase)
    return response, phrase


#print(match_given_rule(rules, "do you want to go outside"))


def respond_exact(responses,message):
    rtn_msg = None
    for key , value in responses.items():
        if key == message:
            rtn_msg = random.choice(value)
    return rtn_msg

def is_question(message):
    reply = None
    if message.endswith("?"):
        reply = "your message was a question"
    return reply

def reply_to_message(message):
    message = message.lower()
    reply = respond_exact(responses, message)
    if reply is None:
        match_response, match_phrase  =match_given_rule(rules,message)
        reply = match_response
        #print("here --->", match_response, match_phrase)
        if match_phrase is None:
            reply = is_question(message)
    
    if reply is None:
        reply = default
    return reply

#print("1.", reply_to_message("hello"))
#print("2.", reply_to_message("do you want to go outside"))
#print("3.", reply_to_message("what is the weather like?"))
#print("4.", reply_to_message("chungus"))
cond = True
while(cond):
    message = input(user_msg)
    msg_reply = bot_msg.format(reply_to_message(message))
    if message == "exit":
        confirm = input("Exit? (yes/no)").lower()
        if confirm == "yes":
            break
        elif confirm == "no":
            print("return to chat")
        else:
            print(confirm, " is not an option, return to chat")
    else:
        print(msg_reply)



