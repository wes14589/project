# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 09:58:15 2019
Rule based chatbot with exact rules, responses, user_questions and defaults.

@author: tomwe

Project tlist of jobs:
- depthen the current rules using online resource (put loads more input options in (hi,hey,whats up, good morning....))
- make the replies more engaging (reply with an answer and a question back to the user)
- answer questions using google search (calculator, converter...)
- ^ google search working for most topics and calculator except for addition problems


"""

import re
import random
import json
import datetime
import requests


print("To exit the program type \"exit\"\nor Hit enter for me to ask a question\n\n")
def readDictionaries():
    global user_msg
    user_msg = "User: "
    global bot_msg
    bot_msg= "Bot: {0}"
    global first_message
    first_message = "Welcome to the future, let's have a chat"
    print(bot_msg.format(first_message))
    global default
    default = "Sorry I did not understand that, here is a question for you\n"
    user_name = ""

    # load the two sets of chat terms
    with open('Dictionaries/responses.json') as f:
        global responses
        responses = json.load(f)
    with open('Dictionaries/rules.json') as f:
        global rules
        rules = json.load(f)
    with open('Dictionaries/user_questions.json') as f:
        global user_questions
        user_questions = json.load(f)
    with open('Dictionaries/topics.json') as f:
        global topics
        topics = json.load(f)
    with open('Dictionaries/bot_questions.json') as f:
        global bot_questions
        bot_questions = json.load(f)
    with open('Dictionaries/user_input.json') as f:
        global user_input
        user_input = json.load(f)
    with open('Dictionaries/call_func.json') as f:
        global call_func
        call_func = json.load(f)

    #print(call_func)
    #input_list = [user_input["user"]]
    global input_list
    input_list = []

def get_name(message, substring):
    if "name" in message:
        user_name = substring
    return user_name



#print(match_given_rule(rules, "do you want to go outside"))

def getTime(message):
    reply = "the time is {0}"
    currentDT = datetime.datetime.now()
    time = "{0}:{1}".format(currentDT.hour,currentDT.minute)
    reply = reply.format(time)
    return reply

def getDate(message):
    reply = "the date is {0}"
    currentDT = datetime.datetime.now()
    date = "{0}-{1}-{2}".format(currentDT.day,currentDT.month,currentDT.year)
    reply = reply.format(date)
    return reply

def getSearchAddress(message):
    search_str = "https://www.google.com/search?client=firefox-b-d&q="
    x2 = ""
    for c in message: # convert spaces to + for website
        if c == ' ':
            x2 = x2 + '+'
        else:
            x2 = x2 + c
    search_str=search_str+x2
    return search_str


def getDataGoogle(message):
    reply = None
    search_str = getSearchAddress(message)
    try:
        r = requests.get(search_str)
        results = []
        for i in range(0, 4):
            try:
                results.append(r.text.split("<span>",5)[i+1] )
            except:
                break
    except:
        reply = "I do not know what {0} is".format(message)
    try:
        result_section = results[0][0:300] # does it always have to be 0?
    except:
        #print("string not big enough")
        return reply
    bad_letters = ["Sr.", "Jr."]
    final_str = ""
    for x in result_section:
        final_str = final_str+x
        if x == '.':
            if final_str[len(final_str)-3:] == bad_letters[0] or final_str[len(final_str)-3:] == bad_letters[1]:
                #contine
                do_nothing = 0
            else:
                break
    if final_str != "":
        reply = final_str
    return reply

def getMathsGoogle(message):
    reply = None
    search_str = getSearchAddress(message)
    r = requests.get(search_str)
    easy_string = r.text
    result_indicator = " = " # the snippet of code in the google source page which indicates where the answer of the maths calc is (EXCEPT FROM CALCULATIONS WITH +)
    ans = re.search(result_indicator,easy_string )
#    if ans is None:
#        print("search did not find anything")
    results= ""
    try:
        results = r.text.split(result_indicator,1)[1]
    except: # IF SEARCH DOES NOT RETURN RESULT, BREAK AND RETURN UNKNOWN MESSAGE
        print("search not worked")
        reply = "I do not know what {0} is".format(message)
        return reply
    result_section = results[0:50] # get rid of most of the rubbish you don't need
    c = 0
    for x in result_section: # find the end of the value
        if x == "<":
            reply = result_section[0:c]
            break
        c+=1
    maths_symbols = ['0','1','2','3','4','5','6','7','8','9','*','/','+','-','(',')']
    if reply is not None:
        for i in reply:
            if i not in maths_symbols:
                print("this is not a maths answer")
                reply = "I do not know the answer to this question"
                return reply
    reply = random.choice([message +" = "+reply,"the answer is {0}".format(reply)])

    return reply

def getValue(message):
    reply = NONE


    return reply

def getLocation(message):
    reply = None
    # find the key used to get the data out of the google page source
    return reply


def getData(message): # depending on the input of the message
    reply = None
    is_maths = True

    maths_symbols = ['0','1','2','3','4','5','6','7','8','9','*','/','+','-','(',')']
    for i in message:
        if i not in maths_symbols:# this is not a calculation
            is_maths = False
        #print("this is not a calculation - ", i) # KEEP REPLY NONE SO THE PROGRAM LOOKS FOR SOMETHING ELSE
    if not is_maths:
        reply = getDataGoogle(message)
    else:
        reply = getMathsGoogle(message)
    return reply

def call_from_json(message):
    reply = None
    for key, value in call_func.items():
        ans = re.search(key, message)
        if ans is not None: #
            #print("ans found in list", key, " - ", value ) # check vals
            if value == "getTime" or value == "getDate":
                search_input = ""
            else:
                search_input = ans.group(1)
            reply = globals()[value](search_input)
            return reply
    return reply

def respond_exact(responses,message): # when the message matches the exact string as one of the preset responses
    rtn_msg = None
    for key , value in responses.items():
        if key == message:
            rtn_msg = random.choice(value)
    return rtn_msg

def match_given_rule(rules, message): # when part of the message matches one of the rules
    response , phrase = "default", None # set default results incase nothing is found
    for key, value in rules.items():
        ans = re.search(key, message)
        if ans is not None:
            #print("Match found, responses in dict: ", value)
            response = random.choice(value) # random value in list
            phrase = ans.group(1)
            if '{0}' in response:
                response = response.format(phrase)
    return response, phrase

def is_question(message):
    reply = None

    for key,value in user_questions.items():
        ans = re.search(key, message)
        if ans is not None:
            reply = random.choice(value)
    if reply is None:
        if message.endswith('?'):
            reply = "I do not know the answer to this question"
    return reply

def show_topic(message):
    reply = None
    for key, value in topics.items():
        ans = re.search(key, message)
        if ans is not None:
            reply = "do you like {0} too?".format(random.choice(value))

    return reply

def reply_to_message(message): # find part of the message that matches with a rule/response and generate the reply
    if message != "": # is the message isn't empty
        reply = respond_exact(responses, message) # get reply for exact rule
        if reply is None:
            match_response, match_phrase = match_given_rule(rules,message) # get reply for a rule
            reply = match_response
            if match_phrase is None:
                reply = is_question(message) # get a reply from a given question (or if message ends in '?')
                if reply is None:
                    reply = show_topic(message) # get reply about a topic (this is not very specific)
                    if reply is None:
                        reply = call_from_json(message)
    else:
        reply = random.choice(bot_questions["question"])
    if reply is None:
        reply = default + random.choice(bot_questions["question"])
    return reply

if __name__ == "__main__":
    readDictionaries()
    # repeat the conversation until user wants to exit
    cond = True
    while(cond):
        message = input(user_msg).lower()

        msg_reply = bot_msg.format(reply_to_message(message))
        if message == "exit":
            confirm = input("Exit? (yes/no) ").lower()
            if confirm == "yes":
                break
            elif confirm == "no":
                print("return to chat")
            else:
                print(confirm, " is not an option, return to chat")
        else:
            if message != "":
                input_list.append(message)
            print(msg_reply)



    user_input["user"] = user_input["user"] + input_list

    with open('Dictionaries/user_input.json', 'w') as outfile:
        json.dump(user_input, outfile)
